export const environment = {
  production: true,
  base_url: '/tcm-envios-ng',
  pdfHomologacaoURL: 'https://virtual.tcm.go.gov.br/envio-manual/api/envio/pdf/homologacao/'
};
