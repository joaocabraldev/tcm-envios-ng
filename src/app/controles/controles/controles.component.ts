import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Subscription } from 'rxjs';
import { Unidade } from 'src/app/consultas/unidades/unidade';
import { UnidadesService } from 'src/app/consultas/unidades/unidades.service';


@Component({
  selector: 'app-controles',
  templateUrl: './controles.component.html',
  styleUrls: ['./controles.component.css']
})
export class ControlesComponent implements OnInit, OnDestroy {
  municipio: number;
  tabela: string;
  layout: string;
  unidade: number;
  carregandoUnidades: boolean;
  unidades: Unidade[];
  ano: number;

  routeSub: Subscription;

  constructor(
    private router: Router,
    private messageService: MessageService,
    private unidadeService: UnidadesService
  ) { }

  ngOnInit(): void {
    this.municipio = parseInt(location.pathname.split('/')[2], 10);
    this.tabelaSelecionada(location.pathname.split('/')[1]);
    this.ano = this.municipio = parseInt(location.pathname.split('/')[3], 10);
  }

  municipioSelecionado(municipio: number): void {
    this.municipio = municipio;
    this.atualizarUnidadesGestoras();
  }

  tabelaSelecionada(tabela: string): void {
    this.tabela = tabela;
    this.atualizarUnidadesGestoras();
  }

  layoutSelecionado(layout: string): void {
    this.layout = layout;
  }

  unidadeSelecionada(unidade: number): void {
    this.unidade = unidade;
  }

  pesquisar(): void {

    this.messageService.clear();

    if (this.municipio === 0) {
      this.adicionarMensagem('Selecione um município!');
    } else if (this.tabela === '') {
      this.adicionarMensagem('Selecione uma tabela!');
    } else if (this.tabela === 'envios' && this.layout === '') {
      this.adicionarMensagem('Selecione um layout!');
    } else if (this.tabela === 'envios' && !this.ano) {
      this.adicionarMensagem('Informe um ano!');
    } else if (this.tabela === 'envios' && !this.unidade) {
      this.adicionarMensagem('Informe uma unidade!');
    } else {
      this.navegar();
    }

  }

  adicionarMensagem(mensagem: string): void {
    this.messageService.clear();
    this.messageService.add({
      severity: 'error',
      summary: mensagem
    });
  }

  private navegar(): void {

    if (this.tabela === 'envios') {
      const url = `/${this.tabela}/${this.municipio}/${this.ano}/${this.unidade}/${this.layout}`;
      this.router.navigate([url]);
    } else {
      const url = `/${this.tabela}/${this.municipio}`;
      this.router.navigate([url]);
    }
  }

  private atualizarUnidadesGestoras(): void {
    if (this.tabela === 'envios') {
      this.carregandoUnidades = true;
      this.unidadeService.getUnidades(this.municipio.toString()).then((unidades) => {
        this.unidades = unidades;
        this.carregandoUnidades = false;
      });
    } else {
      this.unidades = [];
      this.carregandoUnidades = false;
    }
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

}
