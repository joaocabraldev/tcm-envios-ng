import { Component, OnInit, EventEmitter, Output, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';

import { MunicipiosService } from './municipios.service';

@Component({
  selector: 'app-municipios',
  templateUrl: './municipios.component.html',
  styleUrls: ['./municipios.component.css']
})
export class MunicipiosComponent implements OnInit {

  @Output() municipioSelecionadoEvent = new EventEmitter<number>();

  municipioSelecionado: any;
  municipios: any[];

  constructor(private service: MunicipiosService) { }

  ngOnInit(): void {
    this.municipios = this.service.getMunicipios();
    const codigoMunicipio = location.pathname.split('/')[2];


    if (codigoMunicipio) {
      this.municipioSelecionado = codigoMunicipio;
    } else {
      this.municipioSelecionado = 0;
    }
  }

  selecionado(): void {
    this.municipioSelecionadoEvent.emit(this.municipioSelecionado);
  }

}
