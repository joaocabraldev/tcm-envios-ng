import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LayoutsService {

  constructor() { }

  getLayouts(): any[] {
    return [
      { valor: 'ATOSDEPESSOAL', nome: 'ATOS DE PESSOAL' },
      { valor: 'FOLHAPAGAMENTO', nome: 'FOLHA DE PAGAMENTO' }
    ];
  }
}
