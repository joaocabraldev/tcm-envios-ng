import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { LayoutsService } from './layouts.service';

@Component({
  selector: 'app-layouts',
  templateUrl: './layouts.component.html',
  styleUrls: ['./layouts.component.css']
})
export class LayoutsComponent implements OnInit {

  @Output() layoutSelecionadoEvent = new EventEmitter<string>();

  layoutSelecionado: string;
  layouts: any;

  constructor(private service: LayoutsService) { }

  ngOnInit(): void {
    this.layouts = this.service.getLayouts();
    this.layoutSelecionado = location.pathname.split('/')[5];
  }

  selecionado(): void {
    this.layoutSelecionadoEvent.emit(this.layoutSelecionado);
  }

}
