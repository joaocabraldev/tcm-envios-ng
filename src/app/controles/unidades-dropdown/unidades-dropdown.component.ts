import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Unidade } from 'src/app/consultas/unidades/unidade';

@Component({
  selector: 'app-unidades-dropdown',
  templateUrl: './unidades-dropdown.component.html',
  styleUrls: ['./unidades-dropdown.component.css']
})
export class UnidadesDropdownComponent implements OnInit {

  @Input() unidades: Unidade[];

  @Output() unidadeSelecionadaEvent = new EventEmitter<number>();

  unidadeSelecionada: number;

  constructor() { }

  ngOnInit(): void {
    this.unidadeSelecionada = parseInt(location.pathname.split('/')[4], 10);
  }

  selecionada(): void {
    this.unidadeSelecionadaEvent.emit(this.unidadeSelecionada);
  }

}
