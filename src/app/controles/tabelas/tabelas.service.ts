import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TabelasService {

  constructor() { }

  getTabelas(): any[] {
    return [
      { valor: 'unidadesGestoras', nome: 'UNIDADES GESTORAS' },
      { valor: 'legislacoes', nome: 'LEGISLAÇÕES' },
      { valor: 'cargos', nome: 'CARGOS' },
      { valor: 'verbas', nome: 'VERBAS' },
      { valor: 'envios', nome: 'ENVIOS' }
    ];
  }

}
