import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { Subscription } from 'rxjs';

import { TabelasService } from './tabelas.service';

@Component({
  selector: 'app-tabelas',
  templateUrl: './tabelas.component.html',
  styleUrls: ['./tabelas.component.css']
})
export class TabelasComponent implements OnInit {

  @Output() tabelaSelecionadaEvent = new EventEmitter<string>();

  tabelaSelecionada: string;
  tabelas: any;
  sub: Subscription;

  constructor(private service: TabelasService) { }

  ngOnInit(): void {
    this.tabelas = this.service.getTabelas();
    const tabela = location.pathname.split('/')[1];

    if (tabela) {
      this.tabelaSelecionada = tabela;
    } else {
      this.tabelaSelecionada = '';
    }

    console.log('Tabela Component: ', this.tabelaSelecionada);

  }

  selecionado(): void {
    this.tabelaSelecionadaEvent.emit(this.tabelaSelecionada);
  }

}
