import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MenubarModule } from 'primeng/menubar';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { MessageService } from 'primeng/api';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { DialogModule } from 'primeng/dialog';

import { ControlesComponent } from './controles/controles/controles.component';
import { MunicipiosComponent } from './controles/municipios/municipios.component';
import { TabelasComponent } from './controles/tabelas/tabelas.component';

import { HomeComponent } from './home/home.component';
import { UnidadesComponent } from './consultas/unidades/unidades.component';
import { LegislacoesComponent } from './consultas/legislacoes/legislacoes.component';
import { CargosComponent } from './consultas/cargos/cargos.component';
import { CnpjPipe } from './shared/cnpj.pipe';
import { VerbasComponent } from './consultas/verbas/verbas.component';
import { YesnoPipe } from './shared/yesno.pipe';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LegislacaoModalComponent } from './consultas/legislacoes/legislacao-modal/legislacao-modal.component';
import { UnidadeModalComponent } from './consultas/unidades/unidade-modal/unidade-modal.component';
import { CargoModalComponent } from './consultas/cargos/cargo-modal/cargo-modal.component';
import { VerbaModalComponent } from './consultas/verbas/verba-modal/verba-modal.component';
import { LinkReciboComponent } from './shared/link-recibo/link-recibo.component';
import { EnviosComponent } from './consultas/envios/envios.component';
import { LayoutsComponent } from './controles/layouts/layouts.component';
import { UnidadesDropdownComponent } from './controles/unidades-dropdown/unidades-dropdown.component';
import { EnvioModalComponent } from './consultas/envios/envio-modal/envio-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UnidadesComponent,
    LegislacoesComponent,
    CargosComponent,
    MunicipiosComponent,
    TabelasComponent,
    ControlesComponent,
    CnpjPipe,
    VerbasComponent,
    YesnoPipe,
    LegislacaoModalComponent,
    UnidadeModalComponent,
    CargoModalComponent,
    VerbaModalComponent,
    LinkReciboComponent,
    EnviosComponent,
    LayoutsComponent,
    UnidadesDropdownComponent,
    EnvioModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    MenubarModule,
    PanelModule,
    DropdownModule,
    ButtonModule,
    MessagesModule,
    MessageModule,
    TableModule,
    InputTextModule,
    DialogModule,
    FontAwesomeModule
  ],
  providers: [
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
