import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnidadesComponent } from './consultas/unidades/unidades.component';
import { LegislacoesComponent } from './consultas/legislacoes/legislacoes.component';
import { CargosComponent } from './consultas/cargos/cargos.component';
import { HomeComponent } from './home/home.component';
import { VerbasComponent } from './consultas/verbas/verbas.component';
import { EnviosComponent } from './consultas/envios/envios.component';

const routes: Routes = [
  { path: 'unidadesGestoras/:municipio', component: UnidadesComponent },
  { path: 'legislacoes/:municipio', component: LegislacoesComponent },
  { path: 'cargos/:municipio', component: CargosComponent },
  { path: 'verbas/:municipio', component: VerbasComponent },
  { path: 'envios/:municipio/:ano/:unidade/:layout', component: EnviosComponent },
  { path: '', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
