import { Component, OnInit, Input } from '@angular/core';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-link-recibo',
  templateUrl: './link-recibo.component.html',
  styleUrls: ['./link-recibo.component.css']
})
export class LinkReciboComponent implements OnInit {

  @Input() id: string;
  urlRecibo: string;

  constructor() { }

  ngOnInit(): void {
    this.urlRecibo = `${environment.pdfHomologacaoURL}${this.id}`;
  }

}
