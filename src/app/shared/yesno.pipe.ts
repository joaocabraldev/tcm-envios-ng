import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'yesno'
})
export class YesnoPipe implements PipeTransform {

  transform(value: string): string {
    return value ? 'SIM' : 'NÃO';
  }

}
