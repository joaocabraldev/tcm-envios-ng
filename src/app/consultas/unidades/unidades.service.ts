import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { Unidade } from './unidade';

@Injectable({
  providedIn: 'root'
})
export class UnidadesService {

  private baseURL = 'https://ws.tcm.go.gov.br/api/rest/passaporteService/';

  constructor(private http: HttpClient) { }

  async getUnidades(municipio: string): Promise<Unidade[]> {
    const url = `${this.baseURL}${municipio}/unidadesGestoras`;
    return this.http.get(url).toPromise()
      .then(res => res as Unidade[])
      .then(data => data);
  }

}
