import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Unidade } from '../unidade';

@Component({
  selector: 'app-unidade-modal',
  templateUrl: './unidade-modal.component.html',
  styleUrls: ['./unidade-modal.component.css']
})
export class UnidadeModalComponent {

  @Input() unidadeSelecionada: Unidade;
  @Input() visivel: boolean;
  @Output() fecharModal = new EventEmitter<void>();

  constructor() { }

  onFecharModal(): void {
    this.fecharModal.next();
  }

}
