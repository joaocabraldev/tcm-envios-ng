import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { faSearch } from '@fortawesome/free-solid-svg-icons';

import { UnidadesService } from './unidades.service';
import { Unidade } from './unidade';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-unidades',
  templateUrl: './unidades.component.html',
  styleUrls: ['./unidades.component.css']
})
export class UnidadesComponent implements OnInit, OnDestroy {

  constructor(private service: UnidadesService, private route: ActivatedRoute) { }

  faSearch = faSearch;
  municipio: string;
  unidadeSelecionada: Unidade = new Unidade();
  unidades: Unidade[];
  routeSub: Subscription;
  loading: boolean;
  colunas: any[];
  filtroGlobal: string[];
  modal: boolean;

  ngOnInit(): void {

    this.modal = false;
    this.colunas = [
      { field: 'id', header: 'ID' },
      { field: 'nome', header: 'Nome' },
      { field: 'cnpj', header: 'CNPJ' },
      { field: 'tipo', header: 'Tipo' },
      { field: 'dataLeiCriacao', header: 'Data Lei Criação' },
      { field: 'dataCadastro', header: 'Data de Cadastro' },
      { field: 'status', header: 'Status' }
    ];

    this.filtroGlobal = ['id', 'nome', 'cnpj', 'tipo', 'status'];

    this.loading = true;

    this.routeSub = this.route.paramMap.subscribe((params) => {
      this.municipio = params.get('municipio');
      this.service.getUnidades(this.municipio)
        .then(unidades => {
          this.unidades = unidades;
          this.loading = false;
        });
    });

  }

  detalhes(unidade: Unidade): void {
    this.unidadeSelecionada = unidade;
    this.modal = true;
  }

  limparTabela(table: Table, input: HTMLInputElement): void {
    table.clear();
    input.value = '';
  }

  fecharModal(): void {
    this.modal = false;
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

}
