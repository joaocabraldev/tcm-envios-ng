export class Unidade {

    constructor() {}

    id: number;
    nome: string;
    cnpj: string;
    tipo: string;
    dataLeiCriacao: string;
    dataCadastro: string;
    status: string;

}
