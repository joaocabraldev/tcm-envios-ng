import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe } from 'rxjs';
import { take } from 'rxjs/operators';
import { Cargo } from './cargo';

@Injectable({
  providedIn: 'root'
})
export class CargosService {

  private baseURL = 'https://ws.tcm.go.gov.br/api/rest/colareService/';

  constructor(private http: HttpClient) { }

  getCargos(municipio: string): Promise<Cargo[]> {
    const url = `${this.baseURL}${municipio}/cargos`;
    return this.http.get(url).toPromise()
      .then(res => res as Cargo[])
      .then(data => data);
  }

}
