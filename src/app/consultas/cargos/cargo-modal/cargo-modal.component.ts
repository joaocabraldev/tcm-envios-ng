import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Cargo } from '../cargo';

@Component({
  selector: 'app-cargo-modal',
  templateUrl: './cargo-modal.component.html',
  styleUrls: ['./cargo-modal.component.css']
})
export class CargoModalComponent {

  @Input() cargoSelecionado: Cargo;
  @Input() visivel: boolean;
  @Output() fecharModal = new EventEmitter<void>();

  constructor() { }

  onFecharModal(): void {
    this.fecharModal.next();
  }

}
