export class Cargo {

    constructor() {}

    idEnvioColare: string;
    nomeCargo: string;
    dataEnvio: string;
    unidadeGestoraId: number;
    unidadeGestora: string;
    cargaHorariaSemanal: number;
    municipio: string;
    regimeJuridico: string;
    quantitativoCargo: number;
    cbo: string;
    quadroPessoal: string;
    unidadeGestoraRepresentanteId: number;
    unidadeGestoraRepresentante: string;
    representante: string;
    cargoCarreira: boolean;
    cargoAcumulavel: boolean;
    escolaridadeMinima: string;
    idPessoalLegislacao: string;
    situacaoEnvio: string;

}
