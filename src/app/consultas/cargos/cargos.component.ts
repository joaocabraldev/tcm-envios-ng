import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { faSearch } from '@fortawesome/free-solid-svg-icons';

import { CargosService } from './cargos.service';
import { Cargo } from './cargo';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-cargos',
  templateUrl: './cargos.component.html',
  styleUrls: ['./cargos.component.css']
})
export class CargosComponent implements OnInit, OnDestroy {

  constructor(private service: CargosService, private route: ActivatedRoute) { }

  faSearch = faSearch;
  municipio: string;
  cargoSelecionado: Cargo = new Cargo();
  cargos: Cargo[];
  routeSub: Subscription;
  loading: boolean;
  colunas: any[];
  filtroGlobal: string[];
  modal: boolean;

  ngOnInit(): void {

    this.modal = false;

    this.colunas = [
      { field: 'idEnvioColare', header: 'ID' },
      { field: 'nomeCargo', header: 'Nome' },
      { field: 'dataEnvio', header: 'Data Envio' },
      { field: 'unidadeGestora', header: 'Unidade Gestora' },
      { field: 'idPessoalLegislacao', header: 'ID Legislação' },
      { field: 'situacaoEnvio', header: 'Situação' }
    ];

    this.filtroGlobal = [
      'idEnvioColare',
      'nomeCargo',
      'dataEnvio',
      'unidadeGestoraId',
      'unidadeGestora',
      'idPessoalLegislacao'
    ];

    this.loading = true;

    this.routeSub = this.route.paramMap.subscribe((params) => {

      this.municipio = params.get('municipio');

      this.service.getCargos(this.municipio)
      .then(cargos => {
        this.cargos = cargos;
        this.loading = false;
      });

    });

  }

  detalhes(cargo: Cargo): void {
    this.cargoSelecionado = cargo;
    this.modal = true;
  }

  limparTabela(table: Table, input: HTMLInputElement): void {
    table.clear();
    input.value = '';
  }

  fecharModal(): void {
    this.modal = false;
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

}
