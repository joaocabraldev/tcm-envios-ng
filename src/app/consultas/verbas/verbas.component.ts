import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

import { faSearch } from '@fortawesome/free-solid-svg-icons';

import { VerbasService } from './verbas.service';
import { Verba } from './verba';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-verbas',
  templateUrl: './verbas.component.html',
  styleUrls: ['./verbas.component.css']
})
export class VerbasComponent implements OnInit, OnDestroy {

  constructor(private service: VerbasService, private route: ActivatedRoute) { }

  faSearch = faSearch;
  municipio: string;
  verbaSelecionada: Verba = new Verba();
  verbas: Verba[];
  routeSub: Subscription;
  loading: boolean;
  colunas: any[];
  filtroGlobal: string[];
  modal: boolean;

  ngOnInit(): void {
    this.modal = false;

    this.colunas = [
      { field: 'idEnvioColare', header: 'ID' },
      { field: 'nomeVerba', header: 'Nome' },
      { field: 'dataEnvio', header: 'Data Envio' },
      { field: 'unidadeGestora', header: 'Unidade Gestora' },
      { field: 'tipoVerba', header: 'Tipo Verba' },
      { field: 'verbaTransitoria', header: 'Verba Transitória' },
      { field: 'representante', header: 'Representante' },
      { field: 'idPessoalLegislacao', header: 'ID Legislação' },
      { field: 'situacaoEnvio', header: 'Situação' }
    ];

    this.filtroGlobal = ['idEnvioColare', 'nomeVerba', 'dataEnvio', 'idPessoalLegislacao', 'situacaoEnvio'];
    this.loading = true;

    this.routeSub = this.route.paramMap.subscribe((params) => {
      this.municipio = params.get('municipio');
      this.service.getVerbas(this.municipio)
      .then(verbas => {
        this.verbas = verbas;
        this.loading = false;
      });
    });

  }

  detalhes(verba: Verba): void {
    this.verbaSelecionada = verba;
    this.modal = true;
  }

  limparTabela(table: Table, input: HTMLInputElement): void {
    table.clear();
    input.value = '';
  }

  fecharModal(): void {
    this.modal = false;
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

}
