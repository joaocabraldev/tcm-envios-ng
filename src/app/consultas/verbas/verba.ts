export class Verba {

    constructor() {}

    idEnvioColare: string;
    nomeVerba: string;
    tipoVerba: string;
    municipio: string;
    verbaTransitoria: boolean;
    dataEnvio: string;
    quadroPessoal: string;
    unidadeGestoraRepresentante: string;
    unidadeGestoraRepresentanteId: number;
    representante: string;
    dataInicio: string;
    idPessoalLegislacao: number;
    situacaoEnvio: string;

}
