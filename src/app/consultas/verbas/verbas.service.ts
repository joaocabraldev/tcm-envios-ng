import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { Verba } from './verba';

@Injectable({
  providedIn: 'root'
})
export class VerbasService {

  protected tabela = '/verbas';
  private baseURL = 'https://ws.tcm.go.gov.br/api/rest/colareService/';

  constructor(private http: HttpClient) { }

  getVerbas(municipio: string): Promise<Verba[]> {
    const url = `${this.baseURL}${municipio}${this.tabela}`;
    return this.http.get(url).toPromise()
      .then(res => res as Verba[])
      .then(data => data);
  }

}
