import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Verba } from '../verba';

@Component({
  selector: 'app-verba-modal',
  templateUrl: './verba-modal.component.html',
  styleUrls: ['./verba-modal.component.css']
})
export class VerbaModalComponent {

  @Input() verbaSelecionada: Verba;
  @Input() visivel: boolean;
  @Output() fecharModal = new EventEmitter<void>();

  constructor() { }

  onFecharModal(): void {
    this.fecharModal.next();
  }

}
