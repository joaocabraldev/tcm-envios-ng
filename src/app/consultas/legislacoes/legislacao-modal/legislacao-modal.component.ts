import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Legislacao } from '../legislacao';

@Component({
  selector: 'app-legislacao-modal',
  templateUrl: './legislacao-modal.component.html',
  styleUrls: ['./legislacao-modal.component.css']
})
export class LegislacaoModalComponent {

  @Input() legislacaoSelecionada: Legislacao;
  @Input() visivel: boolean;
  @Output() fecharModal = new EventEmitter<void>();

  constructor() { }

  onFecharModal(): void {
    this.fecharModal.next();
  }

}
