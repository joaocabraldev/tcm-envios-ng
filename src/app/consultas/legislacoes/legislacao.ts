export class Legislacao {

    constructor() {}

    idEnvioColare: number;
    tipoNorma: string;
    anoNorma: number;
    numeroNorma: number;
    municipio: string;
    detalhamentoNorma: string;
    ementa: string;
    dataEnvio: string;
    unidadeGestoraId: number;
    unidadeGestora: string;
    situacaoEnvio: string;
    arquivoPrincipalNorma: string;

}
