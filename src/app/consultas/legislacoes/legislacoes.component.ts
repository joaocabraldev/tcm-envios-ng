import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';

import { faSearch } from '@fortawesome/free-solid-svg-icons';

import { Legislacao } from './legislacao';
import { LegislacoesService } from './legislacoes.service';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-legislacoes',
  templateUrl: './legislacoes.component.html',
  styleUrls: ['./legislacoes.component.css']
})
export class LegislacoesComponent implements OnInit, OnDestroy {

  constructor(private service: LegislacoesService, private route: ActivatedRoute) { }

  faSearch = faSearch;
  municipio: string;
  legislacaoSelecionada: Legislacao = new Legislacao();
  legislacoes: Legislacao[];
  routeSub: Subscription;
  loading: boolean;
  colunas: any[];
  filtroGlobal: string[];
  modal: boolean;

  ngOnInit(): void {

    this.modal = false;

    this.colunas = [
      { field: 'idEnvioColare', header: 'ID' },
      { field: 'tipoNorma', header: 'Tipo da Norma' },
      { field: 'numeroNorma', header: 'Número' },
      { field: 'anoNorma', header: 'Ano' },
      { field: 'unidadeGestora', header: 'Unidade Gestora' },
      { field: 'dataEnvio', header: 'Data Envio' },
      { field: 'situacaoEnvio', header: 'Situação' }
    ];

    this.filtroGlobal = ['idEnvioColare', 'tipoNorma', 'numeroNorma', 'anoNorma', 'dataEnvio', 'unidadeGestoraId', 'unidadeGestora'];
    this.loading = true;

    this.routeSub = this.route.paramMap.subscribe((params) => {
      this.municipio = params.get('municipio');
      this.service.getLegislacoes(this.municipio)
      .then(legislacoes => {
        this.legislacoes = legislacoes;
        this.loading = false;
      });
    });

  }

  detalhes(legislacao: Legislacao): void {
    this.legislacaoSelecionada = legislacao;
    this.modal = true;
  }

  limparTabela(table: Table, input: HTMLInputElement): void {
    table.clear();
    input.value = '';
  }

  fecharModal(): void {
    this.modal = false;
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

}
