import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Legislacao } from './legislacao';

@Injectable({
  providedIn: 'root'
})
export class LegislacoesService {

  private baseURL = 'https://ws.tcm.go.gov.br/api/rest/colareService/';

  constructor(private http: HttpClient) { }

  async getLegislacoes(municipio: string): Promise<Legislacao[]> {
    const url = `${this.baseURL}${municipio}/legislacoes`;
    return this.http.get(url).toPromise()
      .then(res => res as Legislacao[])
      .then(data => data);
  }

}
