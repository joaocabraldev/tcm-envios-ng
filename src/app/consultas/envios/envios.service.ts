import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Envio } from './envio';

@Injectable({
  providedIn: 'root'
})
export class EnviosService {

  private baseURL = 'https://ws.tcm.go.gov.br/api/rest/colareService';

  constructor(private http: HttpClient) { }

  async getEnvios(municipio: string, ano: string, unidadeGestora: string, layout: string): Promise<Envio[]> {
    const url = `${this.baseURL}/${municipio}/${ano}/${unidadeGestora}/${layout}/obterEnvios`;

    return this.http.get(url).toPromise()
      .then(res => res as Envio[])
      .then(data => data);
  }

}
