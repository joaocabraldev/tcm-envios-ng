import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Table } from 'primeng/table';
import { Subscription } from 'rxjs';
import { Envio } from './envio';
import { EnviosService } from './envios.service';

@Component({
  selector: 'app-envios',
  templateUrl: './envios.component.html',
  styleUrls: ['./envios.component.css']
})
export class EnviosComponent implements OnInit, OnDestroy {

  faSearch = faSearch;
  municipio: string;
  ano: string;
  unidade: string;
  layout: string;
  envioSelecionado: Envio = new Envio();
  envios: Envio[];

  routeSub: Subscription;
  loading: boolean;
  colunas: any[];
  filtroGlobal: string[];
  modal: boolean;

  constructor(private service: EnviosService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.modal = false;

    this.colunas = [
      { field: 'idEnvioColare', header: 'ID' },
      { field: 'siglaPrestacaoContas', header: 'Sigla Prestação de Contas' },
      { field: 'unidadeGestoraRepresentanteId', header: 'Id Unidade Gestora' },
      { field: 'unidadeGestoraRepresentante', header: 'Unidade Gestora' },
      { field: 'municipio', header: 'municipio' },
      { field: 'anoReferencia', header: 'Ano' },
      { field: 'mesReferencia', header: 'Mês' },
      { field: 'dataEnvio', header: 'Data Envio' },
      { field: 'situacaoEnvio', header: 'Situação' },
      { field: 'siglaLayout', header: 'Sigla Layout' }
    ];

    this.filtroGlobal = ['idEnvioColare',
      'siglaPrestacaoContas',
      'unidadeGestoraRepresentanteId',
      'unidadeGestoraRepresentante',
      'municipio',
      'anoReferencia',
      'mesReferencia',
      'dataEnvio',
      'siglaLayout'
    ];

    this.loading = true;

    this.routeSub = this.route.paramMap.subscribe((params) => {
      this.municipio = params.get('municipio');
      this.ano = params.get('ano');
      this.unidade = params.get('unidade');
      this.municipio = params.get('municipio');
      this.layout = params.get('layout');

      console.log('Tabela Envios: ', params.get('tabela'));

      this.service.getEnvios(this.municipio, this.ano, this.unidade, this.layout).then((envios) => {
        this.envios = envios;
        this.loading = false;
      });
    });
  }

  detalhes(envio: Envio): void {
    this.envioSelecionado = envio;
    this.modal = true;
  }

  limparTabela(table: Table, input: HTMLInputElement): void {
    table.clear();
    input.value = '';
  }

  fecharModal(): void {
    this.modal = false;
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

}
