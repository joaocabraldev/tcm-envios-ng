import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Envio } from '../envio';

@Component({
  selector: 'app-envio-modal',
  templateUrl: './envio-modal.component.html',
  styleUrls: ['./envio-modal.component.css']
})
export class EnvioModalComponent {

  @Input() envioSelecionado: Envio;
  @Input() visivel: boolean;
  @Output() fecharModal = new EventEmitter<void>();

  constructor() { }

  onFecharModal(): void {
    this.fecharModal.next();
  }

}
