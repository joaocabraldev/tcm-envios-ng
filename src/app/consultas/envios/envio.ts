export class Envio {

  constructor() {}

  idEnvioColare: number;
  mesReferencia: number;
  anoReferencia: number;
  municipio: string;
  dataEnvio: string;
  representante: string;
  unidadeGestoraRepresentanteId: number;
  unidadeGestoraRepresentante: string;
  siglaPrestacaoContas: string;
  siglaLayout: string;
  situacaoEnvio: string;

}
