import { Component, OnInit } from '@angular/core';

import { MenuItem } from 'primeng/api';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'tcm-envios-ng';

  items: MenuItem[];

  ngOnInit(): void {
    this.items = [
      {
        label: 'TCM Envios',
        icon: 'pi pi-fw pi-home',
        url: environment.base_url,
        styleClass: 'darkMenu'
      }
    ];
  }

}
