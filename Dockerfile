FROM nginx:latest
COPY /app/dist/tcm-envios-ng /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
